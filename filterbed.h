#ifndef FILTERBED_H
#define FILTERBED_H
#include <string>

using namespace std;

class FilterBed
{
private:
    string getChr(string line);

    long int getStart(string line);

    long int getEnd(string line);

    int getValue(string line);

    //get the starting position from the fasta file
    long int getNumber(string line);
public:
    FilterBed();

    void filterResults(string fastaSource, string regularBed);
};

#endif // FILTERBED_H
