#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <math.h>

#define READ_GAP 6
#define SEQ_TO_COMPARE 4
#define START_SEQ 2
#define FAR_WINDOW 2
#define CLOSE_WINDOW 1
#define GAP 1000
#define DIFF 1
#define EMPTY ""

using namespace std;

#include "analyzebed.h"

AnalyzeBed::AnalyzeBed()
{
    countMax=0;
}

//get chromosome
string AnalyzeBed::getChr(string line)
{
    return line.substr(0, line.find("\t"));
}

//get start number
long int AnalyzeBed::getStart(string line)
{
    int firstTab = line.find("\t")+1;
    string start = line.substr(firstTab, line.find_first_of("\t", firstTab));

    return strtol(start.c_str(), NULL, 0);
}

//get end number
long int AnalyzeBed::getEnd(string line)
{
    int secondTab = line.find("\t")+1;
    secondTab = line.find_first_of("\t", secondTab);
    string end = line.substr(secondTab, line.find_last_of("\t"));

    return strtol(end.c_str(), NULL, 0);
}

//get value
int AnalyzeBed::getValue(string line)
{
    int thirdTab = line.find_last_of("\t");
    string value = line.substr(thirdTab, line.size());

    return strtol(value.c_str(), NULL, 0);
}

//find the sum
unsigned int AnalyzeBed::findSum(vector<double> & values, unsigned int start, unsigned int count)
{
    unsigned int sum = 0;
    for (unsigned int i=start; i < start+count; ++i)
    {
        sum += values[i];
    }
    return sum;
}

vector<int> AnalyzeBed::checkMinMax(vector<double> values, string currentChr, int windowSize, unsigned int drag)
{
    //find current chromosome in file


    vector<int> minMax;
    vector<int> upTrend;
    vector<int> downTrend;
    if (values.size() >= SEQ_TO_COMPARE*drag + windowSize)
    {
        unsigned int firstValue =0, secondValue =0, actualValue =0, thirdValue =0, fourthValue =0;
        for(unsigned int i = 0; i<values.size() - 4*drag - windowSize; ++i)
        {
            //assign values to compare
            firstValue = this->findSum(values, i, windowSize);
            secondValue = this->findSum(values, i+1*drag, windowSize);
            actualValue = this->findSum(values, i+2*drag, windowSize);
            thirdValue = this->findSum(values, i+3*drag, windowSize);
            fourthValue = this->findSum(values, i+4*drag, windowSize);

            if(firstValue > secondValue && secondValue > actualValue  && actualValue  < thirdValue && thirdValue < fourthValue)
            {

                double first = 0, second = 0, third = 0, fourth = 0;
                if (secondValue != 0) first = (double(firstValue)/secondValue);
                if (actualValue!=0) second = (double(secondValue)/actualValue);
                if (actualValue!=0) third = (double(thirdValue)/actualValue);
                if(thirdValue!=0) fourth = (double(fourthValue)/thirdValue);

                double difference = first + second + third + fourth;
                difference /= SEQ_TO_COMPARE;

                if (difference >= DIFF)
                {
                    cout << "min at "<<i<<"\n";
                    minMax.push_back(-(i+2*drag));
                }
                ++diffCount;
                diffMean += difference;
            }
            else
                if(firstValue  < secondValue && secondValue < actualValue && actualValue > thirdValue && thirdValue > fourthValue)
                {

                    double first = 0, second = 0, third = 0, fourth = 0;
                    if (firstValue != 0) first = (double(secondValue)/firstValue);
                    if (secondValue!=0) second = (double(actualValue)/secondValue);
                    if (thirdValue!=0) third = (double(actualValue)/thirdValue);
                    if(fourthValue!=0) fourth = (double(thirdValue)/fourthValue);

                    double difference = first + second + third + fourth;
                    difference/= SEQ_TO_COMPARE;

                    if (difference >= DIFF)
                    {
                        cout<<"max at "<<i<<"\n";
                        minMax.push_back(i+2*drag);
                    }
                    ++diffCount;

                    diffMean += difference;
                }
            //adding trend down
                else
                    if (secondValue>actualValue && actualValue>thirdValue)
                    {
                        downTrend.push_back(i);
                    }
            //adding trend up
                    else
                        if (secondValue<actualValue && actualValue<thirdValue)
                        {
                            upTrend.push_back(i);
                        }
        }
    }
    //if we found min max points
    if (!minMax.empty())
    {
        //if a min max chr wasn't found before or it's smaller than this one
        if (minMaxChr.empty() || minMaxChr.size() < minMax.size())
        {
            minMaxChr = minMax;
            downTrendChr = downTrend;
            upTrendChr = upTrend;
            minMaxChrNum = currentChr;
        }
    }



    return minMax;
}


void AnalyzeBed::findMinMax(const string nameOfFile, unsigned int windowSize, unsigned int drag, bool normalize)
{

    fileName = nameOfFile;


    ifstream infile(fileName.c_str());
    string line;

    getline(infile, line);
    //get over track line
    while (line.find("chr") == string::npos)
        getline(infile, line);


    string firstChr = this->getChr(line);
    long int firstStart = this->getStart(line);
    long int firstEnd = this->getEnd(line);
    double firstValue = this->getValue(line);

    long int lastLocation = firstEnd;

    vector<double> values(1);



    values[0] = firstValue;
    string prevChr = firstChr;


    //parse the values
    while(getline(infile, line) && line!=EMPTY)
    {
        firstChr = this->getChr(line);
        firstStart = this->getStart(line);
        firstEnd = this->getEnd(line);
        firstValue = this->getValue(line);
        //if we are on the same chr
        if (firstChr == prevChr)
        {
            firstValue = sqrt(firstValue);
            values.push_back(firstValue);
            prevChr = firstChr;
        }
        //if we finished the chromosome check min max points
        else
        {
            minMaxList.push_back(this->checkMinMax(values, prevChr, windowSize, drag));
            values.clear();
            values.push_back(firstValue);
            lastLocation = firstEnd;
            prevChr = firstChr;
        }
    }

    infile.close();

    minMaxWindowsToFile(nameOfFile, windowSize);
}

//write down the different window sizes for min max points
void AnalyzeBed::windowsToFile(const string nameOfFile, unsigned int windowSize)
{

    ifstream newFile(fileName.c_str());
    string path = nameOfFile;
    path += "minMaxList.txt";
    ofstream writeMinMax;
    writeMinMax.open(path.c_str());

    //make a new vector with all windows, min/max and those before and after
    vector<int> windowChr;
    for (unsigned int i=0; i< minMaxChr.size(); ++i)
    {
        //add half of SEQ_TO_COMPARE windows before the minMax point
        for (int j=SEQ_TO_COMPARE/2; j > 0; --j)
        {
            //if the vector is empty or the last window is smaller than the one we want to insert - insert it
            if (windowChr.empty() || windowChr[windowChr.size() -1] < abs(minMaxChr[i]) - j)
                windowChr.push_back(abs(minMaxChr[i]) - j);

        }

        //add the min max window
        windowChr.push_back(abs(minMaxChr[i]));

        //add half of SEQ_TO_COMPARE windows after the minMax point
        for (int j=1; j<=SEQ_TO_COMPARE/2; ++j)
        {
            //if this is the last minMax, or the window we want to add is smaller than the next minMax window - add it
            if (i == minMaxChr.size() - 1 || abs(minMaxChr[i]) + j < abs(minMaxChr[i+1]))
                windowChr.push_back(abs(minMaxChr[i]) + j);
        }

    }

    string line;
    //find the right chromosome
    do
    {
        getline(newFile,line);
    }
    while (this->getChr(line)!=minMaxChrNum);

    long lastPosition;

    //find the min max points
    int offset =0;
    for (unsigned int i=0; i< windowChr.size(); ++i)
    {
        string endLine;
        int location = abs(windowChr[i]) - offset;
        offset = abs(windowChr[i]);
        for (int j=0; j< location; ++j)
            getline(newFile,line);

        //remember position
        lastPosition = newFile.tellg();

        //search for the end of window
        for (unsigned int j=0; j < windowSize - 1; ++j)
            getline(newFile, endLine);

        //write data to file
        writeMinMax << getChr(line) << "\t" << getStart(line) << "\t" << getEnd(endLine) << "\n";

        //rewind file
        newFile.seekg(lastPosition);
    }
    writeMinMax.close();
}

//write down seperatly the min and max windows
void AnalyzeBed::minMaxWindowsToFile(const string nameOfFile, unsigned int windowSize)
{
    //open the bed file for reading
    ifstream newFile(fileName.c_str());

    //make a path for the min list
    string pathMin = nameOfFile;
    pathMin += "minList.txt";

    //make a file for the min list
    ofstream writeMin;
    writeMin.open(pathMin.c_str());

    //make a path for the max list
    string pathMax = nameOfFile;
    pathMax += "maxList.txt";

    //make a file for the max list
    ofstream writeMax;
    writeMax.open(pathMax.c_str());

    string line;
    //find the right chromosome
    do
    {
        getline(newFile,line);
    }
    while (this->getChr(line)!=minMaxChrNum);

    long lastPosition;

    //find the min max points
    int offset =0;
    for (unsigned int i=0; i< minMaxChr.size(); ++i)
    {
        string endLine;

        //find the line in original bed file
        int location = abs(minMaxChr[i]) - offset;
        offset = abs(minMaxChr[i]);

        for (int j=0; j< location; ++j)
            getline(newFile,line);

        //remember position
        lastPosition = newFile.tellg();

        //in case windowSize = 1 or any other fault
        endLine = line;

        //search for the end of window
        for (unsigned int j=0; j < windowSize - 1; ++j)
            getline(newFile, endLine);

        //write data to file
        //if min point write to min file
        if (minMaxChr[i] < 0)
            writeMin << getChr(line) << "\t" << getStart(line) << "\t" << getEnd(endLine) << "\n";
        //else write to max file
        else
            writeMax << getChr(line) << "\t" << getStart(line) << "\t" << getEnd(endLine) << "\n";

        //rewind file
        newFile.seekg(lastPosition);
    }
    //close files
    writeMin.close();
    writeMax.close();
    newFile.close();
}

vector<int> AnalyzeBed::getMinMaxChr()
{
    //if we have a min max chr return it
    if (!minMaxChr.empty())
        return minMaxChr;
    else
        throw;
}

vector<int> AnalyzeBed::getUpTrendChr()
{
    //if we have a min max chr return the up trend one
    if (!minMaxChr.empty())
        return upTrendChr;
    else
        throw;
}

vector<int> AnalyzeBed::getDownTrendChr()
{
    //if we have a min max chr return the down trend one
    if (!minMaxChr.empty())
        return downTrendChr;
    else
        throw;
}

void AnalyzeBed::makeVectorFile(const string nameOfFile)
{
    fileName = nameOfFile;


    ifstream infile(fileName.c_str());
    string line;

    getline(infile, line);
    //get over track line
    while (line.find("chr") == string::npos)
        getline(infile, line);

    string path = nameOfFile;

    string thisChr = getChr(line);

    path+= thisChr;

    ofstream writeValueVector;
    writeValueVector.open(path.c_str());

    do
    {
        if (getChr(line) != thisChr)
        {
            thisChr = getChr(line);
            path = nameOfFile;
            path += thisChr;
            writeValueVector.close();
            writeValueVector.open(path.c_str());
        }

        double value = this->getValue(line);
        writeValueVector << value << ",";

    } while(getline(infile, line));

    infile.close();
    writeValueVector.close();
}
