#include "patterns.h"
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <ctime>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <boost/math/distributions/students_t.hpp>
#include <iomanip>

#define MAX '^'
#define MIN 'v'
#define NOTHING '-'
#define UP_TREND 'U'
#define DOWN_TREND 'D'
#define SIGNIFICANCE_COFACTOR 1000

using namespace std;
using namespace boost::math;

Patterns::Patterns(vector<vector <int> > minMaxchromosomes, vector<vector <int> > upTrendChromosomes, vector<vector <int> > downTrendChromosomes)
{

    originalChromosomes = minMaxchromosomes;
    numberOfChr = originalChromosomes.size();

    upTrend = upTrendChromosomes;
    downTrend = downTrendChromosomes;

    //don't pad
    convertToChar();

    //pad
    convertToChar(true);

    //make trend
    convertToCharWithTrend();

    //make trend and pad
    //convertToCharWithTrend(true);

    ofstream writeFile;

//    time_t t = time(0);
//    struct tm * now = localtime(&t);
//    resultPath = now->tm_year + 2000;
//    resultPath += ":";
//    resultPath += now->tm_mon + 1;
//    resultPath += ":";
//    resultPath += now->tm_mday;
    resultPath = "-results.txt";

    writeFile.open(resultPath.c_str());

    writeFile.close();
}

Patterns::~Patterns()
{
    writeFile.close();
}

void Patterns::convertToChar(bool padWithZero)
{

    for (unsigned i=0; i<numberOfChr; ++i)
    {
        vector <int>  tempChr = originalChromosomes[i];

        vector <char> convertedChr;
        for(unsigned j=0; j < tempChr.size()-1; ++j)
        {
            if(tempChr[j] >0)
            {
                convertedChr.push_back(MAX);
            }
            else
            {
                convertedChr.push_back(MIN);
            }

            if (padWithZero)
                for (int start = abs(tempChr[j]+1); start < abs(tempChr[j+1]); ++start)
                    convertedChr.push_back(NOTHING);
        }

        if (padWithZero)
            paddedChromosomes.push_back(convertedChr);
        else
            chromosomes.push_back(convertedChr);
    }
}

void Patterns::convertToCharWithTrend(bool padWithZero)
{
    for (unsigned i=0; i<numberOfChr; ++i)
    {
        //chromosome to work one
        vector <int>  tempChr = originalChromosomes[i];
        //up trend of chr
        vector <int>  tempUp = upTrend[i];
        //down trend of chr
        vector <int>  tempDown = downTrend[i];
        //result chr
        vector <char> convertedChr;

        unsigned int chrSize = tempChr.size();
        unsigned int upSize = tempUp.size();
        unsigned int downSize = tempDown.size();
        unsigned int totalSize = chrSize + upSize +downSize;

        unsigned int chr =0, up =0, down =0;


        for(unsigned j=0; j < totalSize; ++j)
        {
            //if temp chr is the smallest
            if(abs(tempChr[chr]) < tempUp[up] && abs(tempChr[chr]) < tempDown[down])
            {
                if (tempChr[chr] > 0)
                {
                    convertedChr.push_back(MAX);
                }
                else
                {
                    convertedChr.push_back(MIN);
                }

                ++chr;
            }
            //if temp up is the smallest
            else
            if (tempUp[up] < abs(tempChr[chr]) && tempUp[up] < tempDown[down])
            {
                convertedChr.push_back(UP_TREND);

                ++up;
            }
            //if temp down is the smallest
            else
            if (tempDown[down] < abs(tempChr[chr]) && tempDown[down] < tempUp[up])
            {
                convertedChr.push_back(DOWN_TREND);

                ++down;
            }
            else
                cout <<"something weird happened\n";

        }

        //insert converted chr
        trendChromosomes.push_back(convertedChr);
    }
}

double Patterns::checkSignificance(vector<char> & baseChr, vector<char> & targetChr, double score)
{
    //make seed for shuffle function
    srand(time(0));

    vector<char> smallerVec;
    vector<char> largerVec;
    //sort them by size, we will use different permutations of the smaller one for simplicity
    if (baseChr.size() < targetChr.size())
    {
        smallerVec = baseChr;
        largerVec = targetChr;
    }
    else
    {
        smallerVec = targetChr;
        largerVec = baseChr;
    }

    vector<char> randVec = smallerVec;
    vector<double> results;

    unsigned int inferiorCount = 0;
    //build SIGNIFANCE_COFACTOR times different permutations of smallerVec and check if given score is inferior
    for (int i=0; i < SIGNIFICANCE_COFACTOR; ++i)
    {
        //shuffle the vector
        random_shuffle(randVec.begin(), randVec.end());

        //check edit distance
        double currentScore = edit_distance(randVec, largerVec);

        //when using local alignment a higher score is better, so we check how many are higher than given score
        if (currentScore >= score)
            ++inferiorCount;
    }

    double pValue = inferiorCount/SIGNIFICANCE_COFACTOR;

    return pValue;
}

void Patterns::compareOne(vector<char> & baseChr, vector<char> & targetChr, unsigned int baseNum, unsigned int targetNum)
{
    unsigned int result = edit_distance(baseChr, targetChr);

    unsigned int smaller = (baseChr.size() < targetChr.size()) ? baseChr.size() : targetChr.size();

    double score = 0;
    if (result!=0)
        score = (double)result/(double)smaller;
    score *= 100;
    cout << "score for "<<baseNum + 1<<" & "<<targetNum + 1<<"\n";
    cout << (score)<<"\n";

    writeFile << "score for "<<baseNum + 1<<" & "<<targetNum + 1<<"\n";
    writeFile << (score)<<"\n";

    double pValue = checkSignificance(baseChr, targetChr, result);

    cout << "P Value: " << pValue<<"\n";
    writeFile << "P Value: " <<pValue<<"\n";

}

void Patterns::compareAll(bool comparePadded, bool compareTrend)
{
    //open file to write results to
    writeFile.open(resultPath.c_str(), ofstream::out | ofstream::app);

    //mention in file if this is padded
    if (comparePadded)
        writeFile << "compare padded\n";
    else
        writeFile << "compare non padded\n";

    //mention in file if trend
    if (compareTrend)
        writeFile << "compare trend\n";

    //go over each chromosome
    for (unsigned int i=0; i < numberOfChr - 1; ++i)
    {
        //compare each one to the rest
        for (unsigned j=i+1; j < numberOfChr; ++j)
        {
            if (comparePadded)
                if (compareTrend)
                {
                    compareOne(trendPaddedChromosomes[i], trendPaddedChromosomes[j], i,j);
                }
                else
                    compareOne(paddedChromosomes[i], paddedChromosomes[j], i,j);
            else
                if (compareTrend)
                {
                    compareOne(trendChromosomes[i], trendChromosomes[j], i,j);
                }
                else
                    compareOne(chromosomes[i], chromosomes[j], i,j);
        }
    }

    writeFile.close();
}

int Patterns::edit_distance(const vector<char>& base, const vector<char>& target)
{
    int W = -1;
    int Cr = -1;
    int Cm = 1;
    //find sizes
    const size_t len1 = base.size(), len2 = target.size();

    //make vector for each column
    vector<vector<int> > d(len1 + 1, vector<int>(len2 + 1));

    //init columns
    d[0][0] = 0;
    for(unsigned int i = 1; i <= len1; ++i) d[i][0] = 0;
    for(unsigned int i = 1; i <= len2; ++i) d[0][i] = 0;

    int maxScore = 0;

    for(unsigned int i = 1; i <= len1; ++i)
        for(unsigned int j = 1; j <= len2; ++j)
        {
            d[i][j] = max(max(d[i-1][j] + W, d[i][j-1] + W), d[i-1][j-1] + (base[i-1] == target[j-1] ? Cm : Cr) );
            maxScore = max(maxScore, d[i][j]);
        }

    return maxScore;
}

//unsigned int Patterns::edit_distance(const vector<char>& base, const vector<char>& target)
//{
//    //find sizes
//    const size_t len1 = base.size(), len2 = target.size();

//    //make vector for each column
//    vector<unsigned int> col(len2+1), prevCol(len2+1);

//    //init prevcol
//    for (unsigned int i = 0; i < prevCol.size(); i++)
//        prevCol[i] = i;

//    for (unsigned int i = 0; i < len1; i++)
//    {
//        col[0] = i+1;

//        for (unsigned int j = 0; j < len2; j++)
//            col[j+1] = min( min(prevCol[1 + j] + 1, col[j] + 1), prevCol[j] + (base[i]==target[j] ? 0 : 1) );

//        col.swap(prevCol);
//    }
//    //subtracting the difference between the strings to compare
//    unsigned int sizeVar = (len1 > len2) ? (len1-len2) : (len2 - len1);
//    return prevCol[len2] - sizeVar;
//}










//for(unsigned int i=0; i<minMaxList.size(); ++i)
//{
//    writeFile << "chr " << i <<"\n";
//    for(unsigned int j=0; j< vector<int>(minMaxList[i]).size(); ++j)
//    {
//        writeFile << j << " " << vector<int>(minMaxList[i])[j] << "\n";
//    }
//}

//writeFile.close();
