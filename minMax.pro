TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    analyzebed.cpp \
    patterns.cpp \
    expandbed.cpp \
    filterbed.cpp \
    loess.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    analyzebed.h \
    patterns.h \
    expandbed.h \
    filterbed.h \
    loess.h \
    throw.h

