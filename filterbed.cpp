﻿#include "filterbed.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <math.h>

using namespace std;

FilterBed::FilterBed()
{
}

string FilterBed::getChr(string line)
{
    return line.substr(0, line.find("\t"));
}

long int FilterBed::getStart(string line)
{
    int firstTab = line.find("\t")+1;
    string start = line.substr(firstTab, line.find_first_of("\t", firstTab));

    return strtol(start.c_str(), NULL, 0);
}

long int FilterBed::getEnd(string line)
{
    int secondTab = line.find("\t")+1;
    secondTab = line.find_first_of("\t", secondTab);
    string end = line.substr(secondTab, line.find_last_of("\t"));

    return strtol(end.c_str(), NULL, 0);
}

int FilterBed::getValue(string line)
{
    int thirdTab = line.find_last_of("\t");
    string value = line.substr(thirdTab, line.size());

    return strtol(value.c_str(), NULL, 0);
}

long int FilterBed::getNumber(string line)
{
    int doubleDot = line.find_first_of(":");
    int firstMinus = line.find_first_of("-");
    string number = line.substr(doubleDot+1, firstMinus - doubleDot);

    return strtol(number.c_str(), NULL, 0);
}


void FilterBed::filterResults(string fastaSource, string regularBed)
{
    //source for finding valid locations
    ifstream source(fastaSource.c_str());

    //regular bed to take actual data from
    ifstream regular(regularBed.c_str());

    string sourceLine;
    string regularLine;

    //writing vars
    ofstream writeFile;
    string path = regularBed;
    path+=".filtered.bed";
    writeFile.open(path.c_str());


    while(getline(source, sourceLine))
    {
        //if this isn't a match line
        if (sourceLine.find("chr") == string::npos)
        {

        }
        else
        {
        //find a line with chr in the regular bed file
        do
        {
            getline(regular, regularLine);
            //if this isn't a chr data line - write it to file
            if (regularLine.find("chr") == string::npos)
                writeFile << regularLine <<"\n";
        }
        while (regularLine.find("chr") == string::npos);

        }
        long int sourceStart = this->getStart(sourceLine);
        long int regularStart = this->getStart(regularLine);

        while (sourceStart != regularStart)
        {
            //if source is after regular - advance regular
            if (sourceStart > regularStart)
            {
                getline(regular, regularLine);
                regularStart = this->getStart(regularLine);
            }
            else
            {
                getline(source, sourceLine);
                sourceStart = this->getStart(sourceLine);
            }
        }
        if (this->getValue(sourceLine) == 1)
            writeFile << regularLine << "\n";
    }

    source.close();
    regular.close();

    writeFile.close();
}
