#ifndef EXPANDBED_H
#define EXPANDBED_H
#include <string>

using namespace std;

class ExpandBed
{
private:
    string getChr(string line);

    long int getStart(string line);

    long int getEnd(string line);

    int getValue(string line);
public:
    ExpandBed();

    void getData(string fileName);


};

#endif // EXPANDBED_H
