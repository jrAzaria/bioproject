#ifndef PATTERNS_H
#define PATTERNS_H
#include <vector>
#include <fstream>

using namespace std;
class Patterns
{
private:

    ofstream writeFile;

    string resultPath;

    vector<vector <char> > chromosomes;
    vector<vector <char> > paddedChromosomes;
    vector<vector <int> > originalChromosomes;
    vector<vector <int> > upTrend;
    vector<vector <int> > downTrend;
    vector<vector <char> > trendChromosomes;
    vector<vector <char> > trendPaddedChromosomes;

    vector<double> results;

    unsigned int numberOfChr;


    void convertToChar(bool padWithZero = false);

    void convertToCharWithTrend(bool padWithZero = false);

    double scoreByChance(vector<char>);

    double checkSignificance(vector<char> &baseChr, vector<char> &targetChr, double score);

public:
    Patterns(vector<vector <int> > minMaxchromosomes, vector<vector <int> > upTrendChromosomes, vector<vector <int> > downTrendChromosomes);
    ~Patterns();


    void compareAll(bool comparePadded = false, bool compareTrend = 0);

    void compareOne(vector<char> &baseChr, vector<char> &targetChr, unsigned int baseNum, unsigned int targetNum);

    int edit_distance(const vector<char> &base, const vector<char> &target);

    void writeToFile();

};

#endif // PATTERNS_H
