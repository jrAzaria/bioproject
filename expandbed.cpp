#include "expandbed.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <math.h>

#define ADD_BASE 16
#define TAB "\t"
using namespace std;

ExpandBed::ExpandBed()
{
}

string ExpandBed::getChr(string line)
{
    return line.substr(0, line.find("\t"));
}

long int ExpandBed::getStart(string line)
{
    int firstTab = line.find("\t")+1;
    string start = line.substr(firstTab, line.find_first_of("\t", firstTab));

    return strtol(start.c_str(), NULL, 0);
}

long int ExpandBed::getEnd(string line)
{
    int secondTab = line.find("\t")+1;
    secondTab = line.find_first_of("\t", secondTab);
    string end = line.substr(secondTab, line.find_last_of("\t"));

    return strtol(end.c_str(), NULL, 0);
}

int ExpandBed::getValue(string line)
{
    int thirdTab = line.find_last_of("\t");
    string value = line.substr(thirdTab, line.size());

    return strtol(value.c_str(), NULL, 0);
}

void ExpandBed::getData(string fileName)
{

    //reading vars
    ifstream infile(fileName.c_str());
    string line;

    //writing vars
    ofstream writeFile;
    string path = fileName;
    path+=".expand.bed";
    writeFile.open(path.c_str());


    while(getline(infile, line))
    {
        //if this isn't a match line - copy it as is
        if (line.find("chr") == string::npos)
        {
          writeFile << line <<"\n";
        }
        else
        {
        string chr = this->getChr(line);
        long int start = this->getStart(line);
        long int end = this->getEnd(line);
        double value = this->getValue(line);
        if ((start -= ADD_BASE) < 0)
            start = 0;
        end += ADD_BASE;

        writeFile << chr << TAB << start << TAB << end << TAB << value << "\n";
        }
    }

    infile.close();
    writeFile.close();
}

