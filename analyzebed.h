#ifndef ANALYZEBED_H
#define ANALYZEBED_H

#include <string>
#include <stdlib.h>
#include <vector>

using namespace std;

class AnalyzeBed
{
private:
    string fileName;
    vector<vector<int> > minMaxList;
    int countMax;
    vector<int> minMaxChr;
    vector<int> upTrendChr;
    vector<int> downTrendChr;

    string minMaxChrNum;




    void windowsToFile(const string nameOfFile, unsigned int windowSize);
    void minMaxWindowsToFile(const string nameOfFile, unsigned int windowSize);
public:

    static double diffMean;
    static double diffCount;

    AnalyzeBed();

    string getChr(string line);

    long int getStart(string line);

    long int getEnd(string line);

    int getValue(string line);

    vector<int> checkMinMax(vector<double> values, string currentChr, int windowSize, unsigned int drag);

    unsigned int findSum(vector<double> & values, unsigned int start, unsigned int count);

    void writeToFile();

    void findMinMax(const string nameOfFile, unsigned int windowSize, unsigned int drag, bool normalize = false);

    vector<int> getMinMaxChr();

    vector<int> getUpTrendChr();

    vector<int> getDownTrendChr();

    void makeVectorFile(const string nameOfFile);
};

#endif // ANALYZEBED_H





