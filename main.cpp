#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include "analyzebed.h"
#include "patterns.h"
#include "expandbed.h"
#include "filterbed.h"

#include <time.h>


#define READ_GAP 6
#define GAP 1000

using namespace std;

void filter()
{
    //reading vars
    string source;
    string textFileRegular;

    cout <<"please enter path for source\n";

    cin >> source;

    cout <<"please enter text file path for regular\n";

    cin >> textFileRegular;

    ifstream regular(textFileRegular.c_str());
    string regularFile;


    while(getline(regular, regularFile))
    {
        FilterBed * tool = new FilterBed;

        tool->filterResults(source, regularFile);

        delete tool;
    }

    regular.close();
}

void expand()
{
    //reading vars
    string textFile;

    cout <<"please enter text file pat\n";

    cin >> textFile;

    ifstream infile(textFile.c_str());
    string fileName;


    while(getline(infile, fileName))
    {
        ExpandBed * tool = new ExpandBed;

        tool->getData(fileName);

        delete tool;
    }

    infile.close();
}

//use windowsize and define how much to "drag" the window each time
void compare(unsigned int windowSize, unsigned int drag)
{

    cout <<"please enter path of list of file to analyze\n";
    string listPath;

    cin >> listPath;

    string fileName;
//for (unsigned int i=10; i<60; ++i)
//{
    ifstream infile(listPath.c_str());


    vector <vector<int> > minMaxChromosomes;
    vector <vector<int> > upTrendChromosomes;
    vector <vector<int> > downTrendChromosomes;
    while (getline(infile, fileName))
    {

        AnalyzeBed * analyze =  new AnalyzeBed;

        analyze->findMinMax(fileName, windowSize, drag);

        minMaxChromosomes.push_back(analyze->getMinMaxChr());

        upTrendChromosomes.push_back(analyze->getUpTrendChr());

        downTrendChromosomes.push_back(analyze->getDownTrendChr());

        delete analyze;

    }


    infile.close();
//}
    double difference = AnalyzeBed::diffMean/AnalyzeBed::diffCount;

//    Patterns * patt = new Patterns(minMaxChromosomes, upTrendChromosomes, downTrendChromosomes);



//    cout << "comparing non padded\n";
//    patt->compareAll();

//    cout << "comparing padded\n";
//    patt->compareAll(true);

//////    cout<<"compare trend\n";
//////    patt->compareAllTrend();

//    delete patt;
}

double AnalyzeBed::diffCount = 0;
double AnalyzeBed::diffMean = 0;

//extracting a value vector
void valueVector()
{
    cout <<"please enter path of list of file to analyze\n";
    string listPath;

    cin >> listPath;

    string fileName;

    ifstream infile(listPath.c_str());

    while (getline(infile, fileName))
    {

        AnalyzeBed * analyze =  new AnalyzeBed;

        analyze->makeVectorFile(fileName);
        delete analyze;

    }

    infile.close();
}

int main()
{
    compare(30,5);
    return 0;
}

//int main(int argc, char * argv[])
//{
//   if (*argv[1] == 'c')
//       compare();
//   else
//   if (*argv[1] == 'e')
//       expand();
//   else
//   if (*argv[1] == 'f')
//       filter();

//    return 0;
//}
